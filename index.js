var argv = require('optimist').options('dir', {
      'alias': 'd',
      'demand': false,
      'default': process.cwd(),
      'description': 'directory to serve static files from, default is ' + process.cwd()
    }).options('port', {
      'alias': 'p',
      'demand': false,
      'default': 9000,
      'description': 'port to serve static files on, default is 9000'
    }).argv

var express = require('express');
var app = express();

app.use(express.directory(argv.dir, {}));
app.use(express.static(argv.dir, {}));

app.listen(argv.port);
console.log('serving static files from ' + argv.dir + ' on port ' + argv.port);